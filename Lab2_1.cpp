#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#define N 5
#define M 5
int main ()
{
	int matr [N][M];
	int i,j,pol=0,otr=0;
	srand(time(0));
	for (i=0; i<N; i++)
		for (j=0; j<M; j++)
			matr [i][j]=rand()%100-50;
	for (i=0; i<N; i++)
	{
		for (j=0; j<M; j++)
			printf (" %03d", matr[i][j]);
		putchar ('\n');
	}
	for (i=0; i<N; i++)
		for (j=0; j<M; j++)
			if (matr [i][j]>=0)
				pol=pol+1;
			else
				otr=otr+1;
	printf ("positive=%d, negative=%d\n", pol, otr);
	if (pol>otr)
		puts ("Amount of positive numbers is bigger");
	else
		puts ("Amount of negative numbers is bigger");
	putchar ('\n');
	getch ();
	return 0;
}
