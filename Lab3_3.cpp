#define N 5																// WORDS PER STRING

#include <stdio.h>
#include <conio.h>
int main()
{
	char str[256];
	char *start[N], *end[N];
	int i = 0, inWord = 0, k = 0;
	puts("Enter a string:\n");
	fgets(str, 256, stdin);
	while (str[i]) {
		if (str[i] == ' ' && inWord) {									
			end[k] = &str[i - 1];
			inWord = 0;
			k++;
		}
		else if (str[i] != ' ' && !inWord) {								
			inWord = 1;
			start[k] = &str[i];
		};
		i++;
	};
	end[k] = &str[i - 2];												
	for (k; k >= 0; k--) {
		while (start[k] <= end[k]) {
			putchar(*start[k]++);
		};
		printf(" ");
	};
	puts("\n");
	getch();
	return 0;
}